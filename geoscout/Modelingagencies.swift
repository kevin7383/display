//
//  Modelingagencies.swift
//  geoscout
//
//  Created by SMIT KUKADIYA on 16/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct Modelingagencies: View {
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .center) {
            NavigationBarDotOrange()
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                
                
                VStack(alignment: .center) {
                    Text("Modeling agencies")
                        .bold()
                        .font(.system(size: 27 ))
                        .foregroundColor(Color.GSPrimary)
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 20)
                        .padding(.bottom, 0)
                        .multilineTextAlignment(.center)
                }
                VStack(alignment: .leading) {
                    Text("We have selected the best dutch international modeling agencies for you. ")
                        .font(.system(size: 16))
                        .foregroundColor(Color(red: 61/255, green: 59/255, blue: 83/255))
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 60)
                        .padding(.bottom, 20)
                        .multilineTextAlignment(.center)
                }
                
                VStack(alignment: .leading) {
                                   Image("modell")
                                       .frame(minWidth: 0, maxWidth: .infinity)
                                       .padding(.top, 330)
                                    .padding(.leading, 18)
                               }
                              .frame(width: 303, height: 303)
                               .frame(height: 543)
                               .padding(.top, -230)
                VStack(alignment: .center){
                    Text("MODELS1")
                                       .font(.system(size: 8))
                                       .padding(.top,50)
                }
                VStack(alignment: .center){
                    Text("Models1 represents top models such as\n Adriama Lima, Gisele Budchen.")
                                   .font(.system(size: 12))
                                       .padding(.top,8)
                    
                }
               
                
                
//                VStack(alignment: .center) {
//                    Button(action: {}) {
//                     Text("RESEND EMAIL")
//                         .foregroundColor(.white)
//                     .font(.system(size: 18))
//                     }
//                     .frame(width: 300, height: 55)
//                     .background(Color(red: 255/255, green: 179/255, blue: 108/255))
//                     .cornerRadius(22.0)
//                 }
//                 .frame(minWidth: 0, maxWidth: .infinity)
//                 .padding(.top, 240)
                
                VStack(alignment: .center) {
                    NavigationLink(destination: MeasurmentView()) {
                        ForwardIcon(height: 70, cornerRadius: 35)
                    }.foregroundColor(.white)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 50)
                .padding(.bottom, 35)
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct Modelingagencies_Previews: PreviewProvider {
    static var previews: some View {
        Modelingagencies()
    }
}
