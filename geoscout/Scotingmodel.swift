//
//  Scotingmodel.swift
//  geoscout
//
//  Created by SMIT KUKADIYA on 17/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

let instructions1: String = """

1. Enter the details of your friend.

2. Send the link to your friend

3. Your friend signs up and submits his/her photos

4. You get a reward if your friend gets signed to an agency!


"""
struct Scotingmodel: View {
    @EnvironmentObject var navigator: ScreenNavigator

    init() {
        UIScrollView.appearance().bounces = false
    }

    var body: some View {
        VStack(alignment: .center) {
            NavigationBar(showMore: true)
                .background(Color.clear)
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center) {
                    Image("Lady")
                        .resizable()
                        .scaledToFit()
                        .padding(.top, -20)
                    BecomeAModelGuideInstructionView1()
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding(.top, -50)
                    NavigationLink(destination: ModelingagenciesScouting()) {
                        ForwardIcon(height: 70, cornerRadius: 35, backgroundColor: .white, foregroundColor: .GSPrimary)

                    }.padding(.top, -120)
                }
            }.padding(.bottom, 0)
        }
        .edgesIgnoringSafeArea(.bottom)
        .hideNavigationBar()
    }
}

struct Scotingmodel_Previews: PreviewProvider {
    static var previews: some View {
        Scotingmodel().environmentObject(ScreenNavigator())
    }
}


struct BecomeAModelGuideInstructionView1: View {
    var body: some View {
        VStack(alignment: .center) {
            MediumText(text: "Scouting a model\n How it works", textColor: .white, fontSze: 24)
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 50)
                .padding(.trailing, 50)
                .padding(.trailing, 60)
            
            MediumText(text: instructions1, textColor: .white, fontSze: 16)
                .padding(.top, 30)
                .padding(.leading, 30)
                .padding(.trailing, 60)
            Spacer()
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .frame(height: 500)
        .background(Color.GSPrimary)
        .cornerRadius(40, corners: [.topLeft, .topRight])
        .hideNavigationBar()
        
    }
}
//extension View {
//    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
//        clipShape(RoundedCorner(radius: radius, corners: corners))
//    }
//}

//struct RoundedCorner: Shape {
//    var radius: CGFloat = .infinity
//    var corners: UIRectCorner = .allCorners
//
//    func path(in rect: CGRect) -> Path {
//        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        return Path(path.cgPath)
//    }
//}


