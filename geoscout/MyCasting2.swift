//
//  MyCasting2.swift
//  geoscout
//
//  Created by SMIT KUKADIYA on 16/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct MyCasting2: View {
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .center) {
           NavigationBar(showMore: true)
                          //.background(Color.clear)
                          .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                
                
                VStack(alignment: .center) {
                    Text("My casting")
                        .bold()
                        .font(.system(size: 27 ))
                        .foregroundColor(Color.GSPrimary)
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 30)
                        .padding(.bottom, 0)
                        .multilineTextAlignment(.center)
                }
                VStack(alignment: .center) {
                    Text("We will introduce you to the agencies on ")
                        .font(.system(size: 16))
                        .foregroundColor(Color(red: 61/255, green: 59/255, blue: 83/255))
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 30)
                       
                        .multilineTextAlignment(.center)
                    
                    Text("Monday and get your answer on\n Saturday!")
                    .bold()
                    .font(.system(size: 16))
                    .foregroundColor(Color(red: 61/255, green: 59/255, blue: 83/255))
                    .padding([.leading, .trailing], 20)
                    .padding(.top, 0)
                    .padding(.bottom, 20)
                    .multilineTextAlignment(.center)
                }
                
            
                
                VStack(alignment: .leading) {
                  
                        Image("mycasting2")
                            
                                        .frame(minWidth: 0, maxWidth:.infinity)
                                        .padding(.top, 330)
                                        .padding(.leading, 28)
                        
                    
                               }
                                        .frame(height: 543)
                                       
                                        .padding(.top, -195)
                HStack(alignment: .center){
                    Image("model1")
                                                           .frame(width: 50, height: 303)
                                                           .padding(.top, -230)
                                                           .padding(.trailing, 250)
                                           
                }
                HStack(alignment: .center){
                    Image("model2")
                                                           .frame(width: 40, height: 400)
                                                           .padding(.top, -370)
                                                           .padding(.leading, 285)
                                           
                }
        
                
                
//                VStack(alignment: .center) {
//                    Button(action: {}) {
//                     Text("RESEND EMAIL")
//                         .foregroundColor(.white)
//                     .font(.system(size: 18))
//                     }
//                     .frame(width: 300, height: 55)
//                     .background(Color(red: 255/255, green: 179/255, blue: 108/255))
//                     .cornerRadius(22.0)
//                 }
//                 .frame(minWidth: 0, maxWidth: .infinity)
//                 .padding(.top, 240)
                
                VStack(alignment: .center) {
                   Button(action: {}) {
                    Text("READ INTERVIEW")
                        .foregroundColor(.white)
                    .font(.system(size: 18))
                    }
                    .frame(width: 280, height: 55)
                    .background(Color(red: 238/255, green: 92/255, blue: 35/255))
                    .cornerRadius(22.0)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 40)
                .padding(.bottom, 20)
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct MyCasting2_Previews: PreviewProvider {
    static var previews: some View {
        MyCasting2()
    }
}
