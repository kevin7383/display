//
//  UploadImageView.swift
//  geoscout
//
//  Created by Dharmesh Sonani on 14/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI


struct UploadImageView: View {
    
    @State var isShowPicker: Bool = false
    @State var image1: Image? = Image("Example1")
    @State var image2: Image? = Image("Example2")
    @State var image3: Image? = Image("Example3")
    @State var image4: Image? = Image("Example4")
    @State var isShowPhotobtn: Bool = true
    @State var isShowPhotobtn1: Bool = true
    @State var selectedImage: Int = 0
    @State var isCamera: Int? = 0
    
    var body: some View {
        
        
        
        VStack(spacing: 20){
            ScrollView(.vertical, showsIndicators: false) {
                
                HStack(alignment: .center){
                    Button(action: {}) {
                        Image("backArrow")
                            .foregroundColor(.GSPrimary)
                            .padding(.bottom, 10)
                            .padding(.trailing, 40)
                            .frame(width: 50, height: 80)
                        Spacer()
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                            Image("plus")
                        }.foregroundColor(Color.GSPrimary)
                            .padding(.top,-10)
                        Button(action: {}) {
                            Image("ThreeDot")
                                .foregroundColor(.GSPrimary)
                                .padding(.bottom, 10)
                                .padding(.trailing, 40)
                                .frame(width: 50, height: 80)
                            
                        }
                    }
                }.padding(.leading,30)
                    .padding(.bottom,20)
                
                
                
                VStack(alignment: .leading)
                {
                    MediumText(text: "Photos", fontSze: 27)
                        .padding(.trailing, 60)
                        .padding(.leading, 0)
                        
                        .multilineTextAlignment(.leading)
                    
                    HStack {
                        
                        Group {
                            Text("UPLOAD  ")
                                .font(.system(size: 18))
                                .fontWeight(.bold) +
                                
                           
                            Text("4 IMAGES")
                                .foregroundColor(Color.red)
                                .fontWeight(.bold)
                            .font(.system(size: 18))
                        }
                        
                        

                    }
                        
                    .padding(.trailing,150)
                    
                } .padding(.top,10)
                
                
                HStack(spacing: 20)  {
                    // VStack {
                    image1?
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .clipped()
                        .onTapGesture {
                            
               
                            self.selectedImage = 1
                            self.isShowPhotobtn = false
                    }
                    
                    
                    image2?
                        
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .clipped()
                        .onTapGesture {
                            
                            
                            self.selectedImage = 2
                            self.isShowPhotobtn = false
                            
                    }
                    
                }.padding(.top,20)
                    .padding(.leading,20)
                    .padding(.trailing,20)
                HStack(spacing: 20)  {
                    // VStack {
                    image3?
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .clipped()
                        .onTapGesture {
                            self.selectedImage = 3
                            self.isShowPhotobtn = false
                    }
                    
                    
                    image4?
                        
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .clipped()
                        .onTapGesture {
                            self.selectedImage = 4
                            self.isShowPhotobtn = false
                    }
                    
                }.padding(.leading,20)
                    .padding(.trailing,20)
                    
                    .sheet(isPresented: $isShowPicker) {
                        
                        if self.selectedImage == 1{
                            
                            ImagePicker(image: self.$image1, camera: self.$isCamera)
                        }
                        else if self.selectedImage == 2{
                            ImagePicker(image: self.$image2, camera: self.$isCamera)
                        }
                        else if self.selectedImage == 3{
                            ImagePicker(image: self.$image3, camera: self.$isCamera)
                        }
                        else if self.selectedImage == 4{
                            ImagePicker(image: self.$image4, camera: self.$isCamera)
                        }
                        
                }
                
                ZStack{
                    HStack{
                        
                    
                        
                        Button(action: {
                            
                            self.isShowPicker.toggle()
                            self.isCamera = 0
                            self.isShowPhotobtn = true
                        }) {
                            Image("gallery")
                            //.foregroundColor(Color.clear)
                            
                            
                        }
                        
                        Button(action: {
                            self.isShowPicker.toggle()
                            self.isCamera =  1
                            self.isShowPhotobtn = true
                        }) {
                            Image("camera")
                        }
                        
                    }.isHidden(isShowPhotobtn)
                    //.padding(.top,20)
                    HStack(alignment: .center) {
                        NavigationLink(destination: ShareScoutingLink2()) {
                            ForwardIcon(height: 70, cornerRadius: 35)
                        }.foregroundColor(.white)
                            .padding(.top, 10)
                            .isHidden(!isShowPhotobtn)
                    }
                    
                }
                
                
                
            }
            
            
            
        }.hideNavigationBar()
    }
}



struct ImagePicker: UIViewControllerRepresentable {
    
    @Environment(\.presentationMode)
    var presentationMode
    
    @Binding var image: Image?
    @Binding var camera: Int?
    
    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        
        @Binding var presentationMode: PresentationMode
        @Binding var image: Image?
        @Binding var camera: Int?
        
        init(presentationMode: Binding<PresentationMode>, image: Binding<Image?>, camera: Binding<Int?>) {
            _presentationMode = presentationMode
            _image = image
            _camera = camera
        }
        
        func imagePickerController(_ picker: UIImagePickerController,
                                   didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            let uiImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            image = Image(uiImage: uiImage)
            
            
            presentationMode.dismiss()
            
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            presentationMode.dismiss()
        }
        
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(presentationMode: presentationMode, image: $image, camera: $camera)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePicker>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        if camera ==  1
        {
            picker.sourceType = .camera
        }
        else{
            picker.sourceType = .photoLibrary
        }
        
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController,
                                context: UIViewControllerRepresentableContext<ImagePicker>) {
        
    }
    
}


struct UploadImageView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            UploadImageView()
        }
    }
}

extension View {
    
    /// Hide or show the view based on a boolean value.
    ///
    /// Example for visibility:
    /// ```
    /// Text("Label")
    ///     .isHidden(true)
    /// ```
    ///
    /// Example for complete removal:
    /// ```
    /// Text("Label")
    ///     .isHidden(true, remove: true)
    /// ```
    ///
    /// - Parameters:
    ///   - hidden: Set to `false` to show the view. Set to `true` to hide the view.
    ///   - remove: Boolean value indicating whether or not to remove the view.
    func isHidden(_ hidden: Bool, remove: Bool = false) -> some View {
        modifier(HiddenModifier(isHidden: hidden, remove: remove))
    }
}


/// Creates a view modifier to show and hide a view.
///
/// Variables can be used in place so that the content can be changed dynamically.
fileprivate struct HiddenModifier: ViewModifier {
    
    private let isHidden: Bool
    private let remove: Bool
    
    init(isHidden: Bool, remove: Bool = false) {
        self.isHidden = isHidden
        self.remove = remove
    }
    
    func body(content: Content) -> some View {
        Group {
            if isHidden {
                if remove {
                    EmptyView()
                } else {
                    content.hidden()
                }
            } else {
                content
            }
        }
    }
}

//struct UploadImageView: View {
//    var body: some View {
//        VStack(alignment: .leading) {
//            NavigationBar(showMore: true)
//                .background(Color.clear)
//                .frame(height: 60)
//            ScrollView(.vertical, showsIndicators: false) {
//                VStack(alignment: .leading)
//                {
//                    MediumText(text: "Photos", fontSze: 30)
//                                          .padding(.trailing, 60)
//                                          .padding(.leading, 20)
//                                          .padding(.top, 40)
//                                          .multilineTextAlignment(.leading)
//
//                    HStack {
//                        Text("UPLOAD")
//                            .modifier(GSTextStyle(fontWeight: .bold, fontSize: 25)).padding(.leading, 20)
//
//                        Text("4 IMAGES")
//                        .modifier(GSTextStyle(fontWeight: .bold, fontSize: 30))
//                    }.padding(.top, 10)
//
//                    Group{
//                        DishesView().frame(width: UIScreen.main.bounds.size.width-50, height: 460, alignment: .center).padding([.leading, .trailing], 20)
//                    }
//
//                    VStack(alignment: .center) {
//                                                                                            NavigationLink(destination: ShareScoutingLink2()) {
//                                                                                                ForwardIcon(height: 70, cornerRadius: 35)
//                                                                                            }.foregroundColor(.white)
//                                                                                        }
//                                                                                        .frame(minWidth: 0, maxWidth: .infinity)
//                                                                                        .padding(.top, 50)
//
//                }
//            }
//        }
//        .hideNavigationBar()
//        .background(BGColor())
//
//    }
//}
//
//struct UploadImageView_Previews: PreviewProvider {
//    static var previews: some View {
//        UploadImageView()
//    }
//}
//
//struct DishesView: View {
//
//
//    let rows = Row.all()
//
//    var body: some View {
//
//        List {
//
//            ForEach(rows) { row in
//                HStack(alignment: .center) {
//                    ForEach(row.cells) { cell in
//                        Image(cell.imageURL)
//                            .resizable()
//                            .scaledToFit()
//
//                    }
//                }
//            }
//
//        }.padding(EdgeInsets.init(top: 0, leading: -20, bottom: 0, trailing: -20))
//
//    }
//
//}
//extension Row {
//
//    static func all() -> [Row] {
//
//        return [
//
//            Row(cells: [Cell(imageURL: "Example1"), Cell(imageURL: "Example2")]),
//            Row(cells: [Cell(imageURL: "Example3"), Cell(imageURL: "Example4")]),
//
//
//        ]
//
//    }
//
//}
//
//struct Cell: Identifiable {
//    let id = UUID()
//    let imageURL: String
//}
//
//struct Row: Identifiable {
//    let id = UUID()
//    let cells: [Cell]
//}
//
//extension Row {
//
//}
//public struct ImagePickerView: UIViewControllerRepresentable {
//
//    private let sourceType: UIImagePickerController.SourceType
//    private let onImagePicked: (UIImage) -> Void
//    @Environment(\.presentationMode) private var presentationMode
//
//    public init(sourceType: UIImagePickerController.SourceType, onImagePicked: @escaping (UIImage) -> Void) {
//        self.sourceType = sourceType
//        self.onImagePicked = onImagePicked
//    }
//
//    public func makeUIViewController(context: Context) -> UIImagePickerController {
//        let picker = UIImagePickerController()
//        picker.sourceType = self.sourceType
//        picker.delegate = context.coordinator
//        return picker
//    }
//
//    public func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {}
//
//    public func makeCoordinator() -> Coordinator {
//        Coordinator(
//            onDismiss: { self.presentationMode.wrappedValue.dismiss() },
//            onImagePicked: self.onImagePicked
//        )
//    }
//
//    final public class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
//
//        private let onDismiss: () -> Void
//        private let onImagePicked: (UIImage) -> Void
//
//        init(onDismiss: @escaping () -> Void, onImagePicked: @escaping (UIImage) -> Void) {
//            self.onDismiss = onDismiss
//            self.onImagePicked = onImagePicked
//        }
//
//        public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
//            if let image = info[.originalImage] as? UIImage {
//                self.onImagePicked(image)
//            }
//            self.onDismiss()
//        }
//
//        public func imagePickerControllerDidCancel(_: UIImagePickerController) {
//            self.onDismiss()
//        }
//
//    }
//
//}
