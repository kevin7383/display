//
//  SideMenuView.swift
//  geoscout
//
//  Created by Dharmesh Sonani on 15/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct SideMenuView: View {
    @State private var toggleState : Bool = false
    @State private var toggleState2 : Bool = false
    @State private var toggleState3 : Bool = false
    @State private var toggleState4 : Bool = false
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        VStack(alignment: .leading) {
//            GSTitleNavigationBar()
//                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                HStack{
                    Button(action: {
                        self.presentation.wrappedValue.dismiss()
                    }) {
                        Image("backArrow")
                            .foregroundColor(.GSPrimary).padding()
                            .padding(.leading, 0)
                        Spacer()
                    }
                }
                VStack(alignment: .leading) {
                    Group {
                        Text("Hello Jane,")
                            .modifier(GSTextStyle(fontWeight: .medium, fontSize: 27)).padding(.leading, 20)
                        
                        HStack {
                            
                            NavigationLink(destination: Personal_Information()) {
                                ScoutOptionSideMenu(title: "Personal information", imageName: "Diamond", imageColor: .GSDarkText, borderColor: .GSPrimary, iconSize: .init(width: 35, height: 30))
                                
                            }
                            
                        }.padding([.leading, .trailing], 0)
                            .font(.system(size: 16))
                            .padding(.top, 20)
                    }
                    HStack {
                        
                        NavigationLink(destination: LookAndPhysiquesView()) {
                            ScoutOptionSideMenu(title: "Look and physiques", imageName: "Diamond", imageColor: .GSDarkText, borderColor: .GSPrimary, iconSize: .init(width: 35, height: 30))
                            
                        }
                        
                        
                        
                    }.padding([.leading, .trailing], 0)
                         .font(.system(size: 16))
                        .padding(.top, 15)
                    
                    HStack {
                        
                        NavigationLink(destination: Changepassword()) {
                            ScoutOptionSideMenu(title: "Change Password", imageName: "Diamond", imageColor: .GSDarkText, borderColor: .GSPrimary, iconSize: .init(width: 35, height: 30))
                            
                        }
                        
                        
                    }.padding([.leading, .trailing], 0)
                         .font(.system(size: 16))
                        .padding(.top, 15)
                    HStack {
                        Text("Notification Settings")
                            .modifier(GSTextStyle(fontWeight: .medium, fontSize: 27)).padding(.leading, 20)
                        
                    }.padding([.leading, .trailing], 0)
                        .padding(.top, 25)
                    
                    
                    Toggle("", isOn: $toggleState)
                        .toggleStyle(
                            ColoredToggleStyle(label: "Receive email notifications",
                                               onColor: .green,
                                               offColor: .red,
                                               thumbColor: Color(UIColor.white)))
                    
                    Toggle("", isOn: $toggleState2)
                        .toggleStyle(
                            ColoredToggleStyle(label: "Receive Newsletter",
                                               onColor: .purple))
                    
                    
                    Toggle("", isOn: $toggleState3)
                        .toggleStyle(
                            ColoredToggleStyle(label: "Receive Offer Notification",
                                               onColor: .purple))
                    
                    Toggle("", isOn: $toggleState4)
                        .toggleStyle(
                            ColoredToggleStyle(label: "Receive App Updates",
                                               onColor: .purple))
                        .padding(.bottom,10)
                    
                    
                }
            }
        }
        .hideNavigationBar()
        .background(BGColor())
         
    }
}

struct SideMenuView_Previews: PreviewProvider {
    static var previews: some View {
        SideMenuView()
    }
}
struct ScoutOptionSideMenu: View {
    var title: String
    var imageName: String
    var imageColor: Color
    var borderColor: Color
    var iconSize: CGSize
    var body: some View {
        Group{
            VStack {
                VStack {
                    Image(uiImage: UIImage(named: imageName)!.withRenderingMode(.alwaysTemplate))
                        .resizable()
                        .foregroundColor(imageColor)
                        .frame(width: iconSize.width, height: iconSize.height, alignment: .center)
                }
                .frame(width: 70, height: 70)
                .modifier(GSCircularBorder(borderColor: borderColor, borderWidth: 1.5))
                
                
            }.padding(.leading, 20)
            VStack{
                Text(title)
                    .foregroundColor(Color.black)
                    .font(.system(size: 18, weight: .regular, design: .default))
            }.padding(.leading, 30)
            
        }
    }
}
struct ColoredToggleStyle: ToggleStyle {
    var label = ""
    var onColor = Color(UIColor.init(named: "#EE5C23")!)
    var offColor = Color(UIColor.systemGray5)
    var thumbColor = Color.white
    
    func makeBody(configuration: Self.Configuration) -> some View {
        HStack {
            Text(label)
             .font(.system(size: 16))
            Spacer()
            Button(action: { configuration.isOn.toggle() } )
            {
                RoundedRectangle(cornerRadius: 16, style: .circular)
                    .fill(configuration.isOn ? onColor : offColor)
                    .frame(width: 44, height: 23)
                    .overlay(
                        Circle()
                            .fill(thumbColor)
                            .shadow(radius: 1, x: 0, y: 1)
                            .padding(1.5)
                            .offset(x: configuration.isOn ? 10 : -10))
                    .animation(Animation.easeInOut(duration: 0.1))
            }
        }
        .font(.title)
        .padding(.horizontal)
        .padding(.top, 25)
    }
}
