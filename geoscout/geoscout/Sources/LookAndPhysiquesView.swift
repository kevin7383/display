//
//  LookAndPhysiquesView.swift
//  geoscout
//
//  Created by Dharmesh Sonani on 15/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct LookAndPhysiquesView: View {
    @State var email: String = ""
    @State var lastname: String = ""
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .leading) {
           NavigationBarDotOrange()
            .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                
                VStack(alignment: .leading) {
                    
                    Group{
                        
                        MediumText(text: "Look and physiques", fontSze: 22)
                            .padding(.trailing, 60)
                            .padding(.leading, 30)
                            .padding(.top, 15)
                            .multilineTextAlignment(.leading)
                    }
                    
                    Group{
                        HStack{
                            Text("Height")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                                
                            
                            TextField("180", text: $email, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 10)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Dress Size")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("34", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 10)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Shoe size")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("40", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 10)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Haircolor")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("Brown", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 10)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Eyes color")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("Brown", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 10)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Ethinicity")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("Brown", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 10)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    
                    Group{
                        HStack{
                            Text("This makes me unique / know this about me")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                                .font(.system(size: 12))
                        }
                        HStack{
                            Text("i'm a sports athlete since i was 9 years old. i like horses and i'm not affraid of the spotlight.")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                                .font(.system(size: 12))
                        }
                        Divider()
                        
                        VStack(alignment: .center) {
                            NavigationLink(destination: RegistrationView()) {
                                SemiBoldText(text: "SAVE", textColor: .white, fontSze: 14)
                                    .modifier(GSRoundedTextStyle())
                                    .padding([.leading, .trailing], 40)
                                    .padding(.top, 50)
                                    .padding(.bottom,10)
                            }
                        }
                        
                    }
                }
            }
        }.hideNavigationBar()
    }
    
}

struct LookAndPhysiquesView_Previews: PreviewProvider {
    static var previews: some View {
        LookAndPhysiquesView()
    }
}
