//
//  TipsFromExpertView.swift
//  geoscout
//
//  Created by Dharmesh Sonani on 15/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct TipsFromExpertView: View {
    
    var body: some View {
        VStack(alignment: .leading) {
            NavigationBar(showMore: true)
                .background(Color.clear)
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading) {
                    MediumText(text: "Tips from an expert", fontSze: 27)
                        .padding(.trailing, 60)
                        .padding(.leading, 20)
                        .multilineTextAlignment(.leading)
                    
                    Group {
                        SemiBoldText(text: "To summarize", textColor: Color.black, fontSze: 20)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 10)
                            .lineLimit(nil)
                        
                    }
                    Group {
                        RegularText(text: "Agencies do not need professional photos and these summary are just guidelines. If the pictures are not good enough the agencies will always ask for new ones! ", textColor: Color.black, fontSze: 14)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 60)
                            .lineLimit(nil)
                        
                        
                    }
                    Group {
                        RegularText(text: "Ask someone to assist you making the photos;", textColor: .GSPrimary, fontSze: 14)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 25)
                            .lineLimit(nil)
                        
                    }
                    Group {
                        
                        RegularText(text: "Do not wear make-up;", textColor: .GSPrimary, fontSze: 14)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 25)
                            .lineLimit(nil)
                        
                        
                    }
                    Group {
                        
                        RegularText(text: "Wear an outfit you feel comfortable in;", textColor: .GSPrimary, fontSze: 14)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 25)
                            .lineLimit(nil)
                        
                        
                      
                        
                    }
                    Group {
                        
                        RegularText(text: "Make sure to use daylight", textColor: .GSPrimary, fontSze: 14)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 25)
                            .lineLimit(nil)
                  
                        
                    }
                    Group {
                                           
                        RegularText(text: "On the next screen you will find example\n photos!", textColor: Color.black, fontSze: 14)
                                               .padding([.leading, .trailing], 20)
                                               .padding(.top, 60)
                                               .lineLimit(nil)
                        
                                                VStack(alignment: .center) {
                                                                          NavigationLink(destination: ExamplePhotosView()) {
                                                                              ForwardIcon(height: 70, cornerRadius: 35)
                                                                          }.foregroundColor(.white)
                                                                      }
                                                                      .frame(minWidth: 0, maxWidth: .infinity)
                                                                      .padding(.top, 40)
                                                //.padding(.bottom,10)
                                     
                                           
                                       }
                    
                }
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct TipsFromExpertView_Previews: PreviewProvider {
    static var previews: some View {
        TipsFromExpertView()
    }
}
