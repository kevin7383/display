//
//  Personal Information.swift
//  geoscout
//
//  Created by SMIT KUKADIYA on 15/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct Personal_Information: View{
    @State var email: String = ""
    @State var lastname: String = ""
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .leading) {
           NavigationBarDotOrange()
            .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                
                VStack(alignment: .leading) {
                    
                    Group{
                        
                        MediumText(text: "Personal Information", fontSze: 22)
                            .padding(.trailing, 60)
                            .padding(.leading, 30)
                            .padding(.top, 15)
                            .multilineTextAlignment(.leading)
                    }
                    
                    Group{
                        HStack{
                            Text("First Name")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            
                            TextField("Smit", text: $email, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 20)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Last Name")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("avaiya", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 20)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Birth-day")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("28-08-1999", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 25)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Emailaddress")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("smit@gmail.com", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 0)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Email parents")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("parents@gmail.com", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 0)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Phone")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("9078765544", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 53)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Gender")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("Male", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 45)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("Country")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("India", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 40)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                    }
                    Group{
                        HStack{
                            Text("City")
                                
                                .foregroundColor(Color.gray)
                                .padding(.trailing, 20)
                                .padding(.leading, 30)
                                .padding(.top, 20)
                                .multilineTextAlignment(.leading)
                            TextField("Surat", text: $lastname, onEditingChanged: { changed in
                                print("Username onEditingChanged - \(changed)")
                            }) {
                                print("Username onCommit")
                            }.padding(.leading, 70)
                                .padding(.top, 20)
                            
                        }.font(.system(size: 12))
                        Divider()
                        
                        VStack(alignment: .center) {
                            NavigationLink(destination: RegistrationView()) {
                                SemiBoldText(text: "SAVE", textColor: .white, fontSze: 16)
                                    .modifier(GSRoundedTextStyle())
                                    .padding([.leading, .trailing], 40)
                                    .padding(.top, 50)
                                    .padding(.bottom,20)
                            }
                        }
                        
                    }
                }
            }
        }.hideNavigationBar()
    }
    
}


struct Personal_Information_Previews: PreviewProvider {
    static var previews: some View {
        Personal_Information()
    }
    
}
