//
//  MeasurmentView.swift
//  geoscout
//
//  Created by Dharmesh Sonani on 13/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct MeasurmentView: View {
    
    var body: some View {
        VStack(alignment: .leading) {
            NavigationBar(showMore: true)
                .background(Color.clear)
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading) {
                    MediumText(text: "Measurements", fontSze: 30)
                        .padding(.trailing, 60)
                        .padding(.leading, 20)
                        .multilineTextAlignment(.leading)
                    SemiBoldText(text: "Height", textColor: Color.black, fontSze: 13)
                    .padding([.leading, .trailing], 20)
                    .padding(.top, 30)
                    .lineLimit(nil)
                    HStack{
                    Group {
                        
                        
                        SliderView()
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding(.leading, 20)
                            .padding(.trailing, 20)
                        Spacer()
                        SemiBoldText(text: "181 CM", textColor: Color.gray, fontSze: 16)
                        .padding([.trailing], 20)
                        .padding(.top, 5)
                        .lineLimit(nil)
                        }
                        
                    }
                    Divider()
                    Group {
                        SemiBoldText(text: "Dress Size", textColor: Color.black, fontSze: 13)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 0)
                            .lineLimit(nil)
                        HStack{
                        SliderView()
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding(.leading, 20)
                            .padding(.trailing, 20)
                        Spacer()
                            SemiBoldText(text: "34 EU", textColor: Color.gray, fontSze: 16)
                            .padding([.trailing], 20)
                            .padding(.top, 5)
                            .lineLimit(nil)
                        }
                    }
                    Divider()
                    Group {
                        SemiBoldText(text: "Shoe Size", textColor: Color.black, fontSze: 13)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 0)
                            .lineLimit(nil)
                        HStack{
                        SliderView()
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding(.leading, 20)
                            .padding(.trailing, 20)
                        Spacer()
                            SemiBoldText(text: "43 EU", textColor: Color.gray, fontSze: 16)
                            .padding([.trailing], 20)
                            .padding(.top, 5)
                            .lineLimit(nil)
                        }
                    }
                    Divider()
                    Group {
                        
                        SemiBoldText(text: "Haircolor", textColor: Color.black, fontSze: 13)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 0)
                            .lineLimit(nil)
                        
                        textFieldView()
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding(.leading, 20)
                            .padding(.trailing, 20)
                        
                    }
                    
                    Group {
                        
                        SemiBoldText(text: "Eyes color", textColor: Color.black, fontSze: 13)
                            .padding([.leading, .trailing], 20)
                            .padding(.top, 0)
                            .lineLimit(nil)
                        
                        textFieldViewEyeColor()
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding(.leading, 20)
                            .padding(.trailing, 20)
                        
                        
                        VStack(alignment: .center) {
                            NavigationLink(destination: ExpertTipsView()) {
                                ForwardIcon(height: 70, cornerRadius: 35)
                            }.foregroundColor(.white)
                        }
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding(.top, 5)
                        .padding(.bottom,10)
                        
                    }
                    
                }
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct MeasurmentView_Previews: PreviewProvider {
    static var previews: some View {
        MeasurmentView()
    }
}
struct SliderView: View {
    
    @State var sliderValue: Double = 0
    
    var body: some View {
        HStack(alignment: .top) {
            
            SwiftUISlider(
                thumbColor: UIColor.init(hexString: "#EE5C23")!,
                minTrackColor: UIColor.init(hexString: "#EE5C23")!,
                maxTrackColor: UIColor.lightGray,
                value: $sliderValue
            ) .padding(.leading, 10)
                .padding(.trailing, 20)
            
            
            
            //            Slider(value: $sliderValue, in: 0...20, step: 1)
            //                .accentColor(.GSPrimary)
            //                .padding(.leading, 10)
            //                .padding(.trailing, 100)
            
            
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .frame(height: 80)
        .background(Color.white)
        .cornerRadius(10, corners: [.topLeft, .topRight, .bottomLeft, .bottomRight])
        .hideNavigationBar()
    }
}

struct textFieldView: View {
    
    @State var hairColor: String = ""
    @State var EyeColor: String = ""
    
    var body: some View {
        HStack(alignment: .top) {
            
            TextField("Brown", text: $hairColor, onEditingChanged: { changed in
                print("Username onEditingChanged - \(changed)")
            }) {
                print("Username onCommit")
            }.padding(.top, 0)
                .padding([.leading, .trailing], 20)
                .frame(height: 80)
                .font(Font.system(size: 16, weight: .medium, design: .default))
            
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .frame(height: 60)
        .background(Color.white)
        .overlay(
            RoundedRectangle(cornerRadius: 5)
                .stroke(Color.gray, lineWidth: 0.5)
            
        )
            .hideNavigationBar()
    }
}

struct textFieldViewEyeColor: View {
    
    @State var EyeColor: String = ""
    
    var body: some View {
        HStack(alignment: .top) {
            
            TextField("Green", text: $EyeColor, onEditingChanged: { changed in
                print("Username onEditingChanged - \(changed)")
            }) {
                print("Username onCommit")
            }.padding(.top, 0)
                .padding([.leading, .trailing], 20)
                .frame(height: 80)
                .font(Font.system(size: 16, weight: .medium, design: .default))
            
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .frame(height: 60)
        .background(Color.white)
        .overlay(
            RoundedRectangle(cornerRadius: 5)
                .stroke(Color.gray, lineWidth: 0.5)
        )
            .hideNavigationBar()
    }
}

struct SwiftUISlider: UIViewRepresentable {
    
    final class Coordinator: NSObject {
        // The class property value is a binding: It’s a reference to the SwiftUISlider
        // value, which receives a reference to a @State variable value in ContentView.
        var value: Binding<Double>
        
        // Create the binding when you initialize the Coordinator
        init(value: Binding<Double>) {
            self.value = value
        }
        
        // Create a valueChanged(_:) action
        @objc func valueChanged(_ sender: UISlider) {
            self.value.wrappedValue = Double(sender.value)
        }
    }
    
    var thumbColor: UIColor = .white
    var minTrackColor: UIColor?
    var maxTrackColor: UIColor?
    
    @Binding var value: Double
    
    func makeUIView(context: Context) -> UISlider {
        let slider = UISlider(frame: .zero)
        slider.minimumTrackTintColor = minTrackColor
        slider.maximumTrackTintColor = maxTrackColor
        slider.value = Float(value)
       // slider.setThumbImage(UIImage.init(named: "handle"), for: .normal)
      //  slider.setMaximumTrackImage(UIImage.init(named: "slide-bg"), for: .normal)
      //  slider.setMinimumTrackImage(UIImage.init(named: "slide-active-bg"), for: .normal)
        slider.addTarget(
            context.coordinator,
            action: #selector(Coordinator.valueChanged(_:)),
            for: .valueChanged
        )
        
        return slider
    }
    
    func updateUIView(_ uiView: UISlider, context: Context) {
        // Coordinating data between UIView and SwiftUI view
        uiView.value = Float(self.value)
    }
    
    func makeCoordinator() -> SwiftUISlider.Coordinator {
        Coordinator(value: $value)
    }
}
