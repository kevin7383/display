//
//  GSOnboardView.swift
//  geoscout
//
//  Created by Ajay Mehra on 31/05/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct GSOnboardView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct GSOnboardView_Previews: PreviewProvider {
    static var previews: some View {
        GSOnboardView()
    }
}
