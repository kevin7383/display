//
//  FAQViewDetail.swift
//  geoscout
//
//  Created by Smit Kukadiya on 20/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct FAQViewDetail: View {
    
    var body: some View {
        
        VStack(alignment: .center) {
                
            ScrollView(.vertical, showsIndicators: false) {
                
                ContentViewSliderFAQ()
                HelpModel()
                GBimage()
                OnlyText()
                Lineimage()
                ButtomText()
            }
           
        }.hideNavigationBar()
     //   .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)

    }
}

struct FAQViewDetail_Previews: PreviewProvider {
    static var previews: some View {
        FAQViewDetail()
    }
}



struct ContentViewSliderFAQ: View {
    @State var index = 0
     @Environment(\.presentationMode) var presentation
    var images = ["faq", "faq", "faq", "faq"]
    
    var body: some View {
        
        ZStack{
            PagingViewFAQ(index: $index.animation(), maxIndex: images.count - 1) {
                ForEach(self.images, id: \.self) { imageName in
                    Image(imageName)
                        .resizable()
                        .scaledToFill()
                }
            }
            .frame(width: UIScreen.main.bounds.width, height: 451)
            
            // .aspectRatio(1.0, contentMode: .fit)
            
            HStack {
                Button(action: {
                    
                    self.presentation.wrappedValue.dismiss()

                }) {
                    Image("backArrow")
                        .foregroundColor(.white).padding()
                }
                Spacer()
                
                Button(action: {}) {
                    Image("xxx")
                        .foregroundColor(.white).padding()
                }
                
                Button(action: {}) {
                    Image("ThreeDot")
                        .foregroundColor(.white).padding()
                }
               
                .padding(.trailing, 5)
                
            }.padding(.bottom, 340)
            
        }
        .padding()
       
    }
}


struct PagingViewFAQ<Content>: View where Content: View {

    @Binding var index: Int
    let maxIndex: Int
    let content: () -> Content
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>

    @State private var offset = CGFloat.zero
    @State private var dragging = false

    init(index: Binding<Int>, maxIndex: Int, @ViewBuilder content: @escaping () -> Content) {
        self._index = index
        self.maxIndex = maxIndex
        self.content = content
    }

    var body: some View {
        ZStack(alignment: .bottom) {
            GeometryReader { geometry in
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(spacing: 0) {
                        self.content()
                            .frame(width: geometry.size.width, height: geometry.size.height)
                            .clipped()
                    }
                }
                .content.offset(x: self.offset(in: geometry), y: 0)
                .frame(width: geometry.size.width, alignment: .leading)
                .gesture(
                    DragGesture().onChanged { value in
                        self.dragging = true
                        self.offset = -CGFloat(self.index) * geometry.size.width + value.translation.width
                    }
                    .onEnded { value in
                        let predictedEndOffset = -CGFloat(self.index) * geometry.size.width + value.predictedEndTranslation.width
                        let predictedIndex = Int(round(predictedEndOffset / -geometry.size.width))
                        self.index = self.clampedIndex(from: predictedIndex)
                        withAnimation(.easeOut) {
                            self.dragging = false
                        }
                    }
                )
            }
            .clipped()
           
            PageControlViewFAQ(index: $index, maxIndex: maxIndex)
           
        }
       
    }

    func offset(in geometry: GeometryProxy) -> CGFloat {
        if self.dragging {
            return max(min(self.offset, 0), -CGFloat(self.maxIndex) * geometry.size.width)
        } else {
            return -CGFloat(self.index) * geometry.size.width
        }
    }

    func clampedIndex(from predictedIndex: Int) -> Int {
        let newIndex = min(max(predictedIndex, self.index - 1), self.index + 1)
        guard newIndex >= 0 else { return 0 }
        guard newIndex <= maxIndex else { return maxIndex }
        return newIndex
    }
}

struct PageControlViewFAQ: View {
    @Binding var index: Int
    let maxIndex: Int

    var body: some View {
        HStack(spacing: 8) {
            ForEach(0...maxIndex, id: \.self) { index in
                Circle()
                    .fill(index == self.index ? Color.white : Color.gray)
                    .frame(width: 10, height: 10)
                    .padding(.bottom, 20)
            }
        }
        .padding(15)
    }
}

struct HelpModel: View {
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Text("Help my daughter/son wants to \nbecome a model")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .bold, fontSize: 12))
                .lineLimit(nil)
            
            Text("The modeling world is a world that has a lot of \nattraction to it. Both for models as for scouters.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut\ntincidunt ligula sit amet sodales fermentum. Proin sed ligula\ngravida, vulputate enim eget, varius sem. Nullam tincidunt\naugue et volutpat condimentum. Duis varius orci magna,\nposuere mollis ipsum placerat non. Sed at nisi tincidunt,\ntempor lorem at, faucibus libero. Donec eleifend sit amet nisl\nat suscipit. Vestibulum ornare ornare ipsum nec elementum.\nDonec lobortis neque ut molestie auctor. Donec euismod\nlorem non est scelerisque efficitur.")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .regular, fontSize: 13))
                .lineLimit(nil)
                .padding(.top, 20)
                .frame(width: 380)
                .frame(height: 200)
        }
        .frame(minWidth: 0, maxWidth: .infinity)
     
        .padding(.trailing, -10)
    }
}

struct GBimage: View {
    
    var body: some View {
        
        VStack(alignment: .leading) {
            Image("gb")
                                                                      .frame(width: 315, height: 153)
                                                                      .padding(.top, 40)
                                                                     
           
        }
        
    }
}

struct OnlyText: View {
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut \ntincidunt ligula sit amet sodales fermentum. Proin sed ligula \ngravida, vulputate enim eget, varius sem. Nullam tincidunt \naugue et volutpat condimentum. Duis varius orci magna,\nposuere mollis ipsum placerat non. Sed at nisi tincidunt, \ntempor lorem at, faucibus libero. Donec eleifend sit amet nisl \nat suscipit. ")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontSize: 13))
                .lineLimit(nil)
                .padding(.top,40)
            
            
        }
        .frame(minWidth: 0, maxWidth: .infinity)
     
        .padding(.trailing, -10)
    }
}

struct Lineimage: View {
    
    var body: some View {
        
        VStack(alignment: .leading) {
            Image("Line")
                                                                      .frame(width: 415, height: 10)
                                                                      .padding(.top, 60)
                                                                     
           
        }
        
    }
}



struct ButtomText: View {
    
    var body: some View {
        
        VStack(alignment: .center) {
            
            Text("Still got a question? Mail us!")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .bold, fontSize: 18))
                .lineLimit(nil)
                .padding(.top,40)
            Spacer()
            
            Text("ikben@gescout.nl")
                           .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255),  fontSize: 18))
                           .lineLimit(nil)
                           .padding(.bottom,50)
            
            
        }
        .frame(minWidth: 0, maxWidth: .infinity)
     
        .padding(.trailing, -10)
    }
}
