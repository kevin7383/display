//
//  ShareScoutingLink2.swift
//  geoscout
//
//  Created by Smit Kukadiya on 15/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct ShareScoutingLink2: View {
    @EnvironmentObject var navigator: ScreenNavigator
     @Environment(\.presentationMode) var presentation
    
  var body: some View {
        VStack(alignment: .center) {
//            NavigationBarDot()
//                .frame(height: 60)
//                .background(Color.GSPrimary)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center) {
                    HStack {
                        Button(action: {
                            self.presentation.wrappedValue.dismiss()
                            
                        }) {
                            Image("backArrow")
                                .foregroundColor(.white).padding()
                        }
                        Spacer()
                        Button(action: {}) {
                            Image("ThreeDot")
                                .foregroundColor(.white).padding()
                        }
                       
                        .padding(.top,20 )
                        
                    }.padding(.top, -5)
                    
                    Image("ic_12")
                    .resizable()
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.top, 0)
                    .padding([.leading, .trailing], 40)
                    .background(Color.GSPrimary)
                    
                    
                    
                                   
                    Text("On Monday we\n introduce you to\n the agencies!")
                        .font(.system(size: 30))
                        .foregroundColor(Color.white)
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 18)
                        .padding(.bottom, 50)
                        .multilineTextAlignment(.center)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .frame(height: 550)
                .background(Color.GSPrimary)
                 .cornerRadius(40, corners: [.bottomLeft, .bottomRight])
                .padding(.top, 0)
                VStack(alignment: .center) {
                      Text("And on Saturday we tell\n you the outcomes!")
                      .font(.system(size: 24
                        ))
                      .foregroundColor(Color(red: 61/255, green: 59/255, blue: 83/255))
                      .padding([.leading, .trailing], 20)
                      .padding(.top, 60)
                      .padding(.bottom, 10)
                      .multilineTextAlignment(.center)
                    
                }
                VStack(alignment: .center) {
                        NavigationLink(destination: ShareScoutingLink4()) {
                            ForwardIcon(height: 70, cornerRadius: 35)
                        }.foregroundColor(.white)
                    }
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.top, 0)
                    .padding(.bottom, 35)
                }
        }
        .hideNavigationBar()
        .background(BGColor())
    }
}

struct ShareScoutingLink2_Previews: PreviewProvider {
    static var previews: some View {
        ShareScoutingLink2()
    }
}
