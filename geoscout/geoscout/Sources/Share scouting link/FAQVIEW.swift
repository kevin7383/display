//
//  FAQVIEW.swift
//  geoscout
//
//  Created by Smit Kukadiya on 20/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct FAQVIEW: View {
    @EnvironmentObject var navigator: ScreenNavigator
    @State var selection: Int? = 0

    var body: some View {
        VStack(alignment: .leading) {
            NavigationBarDotOrange()
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading) {
                    MediumText(text: "ALL TO KNOW ABOUT \nMODELING", textColor: Color(red: 62/255, green: 59/255, blue: 83/255), fontSze: 27)
                        .padding(.trailing, 0)
                        .padding(.leading, 0)
                        .multilineTextAlignment(.leading)
                }.padding(.leading, -100)
                
                ScrollView(.horizontal, showsIndicators: false) {
                    NavigationLink(destination: FAQViewDetail(), tag: 2, selection: $selection) {

                    HStack {
                        VStack{
                            
                            Image("123")
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .padding(.leading, 20)
                            
                            Text("Parents").foregroundColor(Color.black).padding(.leading, 20)
                            
                        }
                            
        
                        
                        VStack{
                            Image("123")
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .padding(.leading, 20)
                            Text("Agencies").foregroundColor(Color.black).padding(.leading, 20)
                        }
                        
                        VStack{
                            Image("123")
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .padding(.leading, 20)
                            Text("Modeling").foregroundColor(Color.black).padding(.leading, 20)
                        }
                        
                        VStack{
                            Image("123")
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .padding(.leading, 20)
                            Text("Scouting").foregroundColor(Color.black).padding(.leading, 20)
                        }
                        
                    }
                    
                    }.hideNavigationBar()
                    
                    
                }
                
                QuestionView()
                PayView()
                WhatHappen()
                Parrents()
            }
            
        }
        .hideNavigationBar()
        .background(BGColor())
    }
}

struct FAQVIEW_Previews: PreviewProvider {
    static var previews: some View {
        FAQVIEW()
    }
}

struct QuestionView: View {
    
    var body: some View {
        
        VStack(alignment: .leading) {
            Text("FREQUENTLY \nASKED QUESTIONS")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .light, fontSize: 20))
                .lineLimit(nil)
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .frame(height: 100)
        .padding(.trailing, 180)
    }
}

struct PayView: View {
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Text("What do we have to pay?")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .bold, fontSize: 16))
                .lineLimit(nil)
            
            Text("Absolutely nothing. We are paid by agencies for\nfinding models.")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .regular, fontSize: 12))
                .lineLimit(nil)
                .padding(.top, 20)
                .frame(width: 270)
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .frame(height: 100)
        .padding(.trailing, 70)
    }
}

struct WhatHappen: View {
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Text("What happens when I’m invited?")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .bold, fontSize: 16))
                .lineLimit(nil)
            
            Text("When you’re invited by a modeling agency we\ncontact you to arrange a casting between you\nand the agency.")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .regular, fontSize: 12))
                .lineLimit(nil)
                .padding(.top, 20)
                .frame(width: 270)
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .frame(height: 100)
        .padding(.trailing, 70)
    }
}

struct Parrents: View {
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Text("Are my parents allowed to come with me?")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .bold, fontSize: 16))
                .lineLimit(nil)
            
            Text("Most certainly, aside from the fact that it’s\nmandatory for minors, agencies love parents!")
                .modifier(GSTextStyle(textColor: Color(red: 61/255, green: 59/255, blue: 83/255), fontWeight: .regular, fontSize: 12))
                .lineLimit(nil)
                .padding(.top, 20)
                .frame(width: 270)
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .frame(height: 100)
        .padding(.trailing, 25)
    }
}
