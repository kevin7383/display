//
//  ExamplePhotosView.swift
//  geoscout
//
//  Created by Smit Kukadiya on 16/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct ExamplePhotosView: View {
    @State var selection: Int? = 0
    @State var man: String? = ""
    @State var woman: String? = ""
    
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .center) {
            NavigationBarDotOrange()
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading) {
                    MediumText(text: "Example photos", fontSze: 27)
                        .padding(.trailing, 60)
                        .padding(.leading, 30)
                        .multilineTextAlignment(.leading)
                    HStack {
                    //    NavigationLink(destination: RegistrationView(), tag: 2, selection: $selection) {
                            
                            Text("Women")
                                .modifier(GSTextStyle(textColor: .GSPrimary, fontWeight: .semibold, fontSize: 16))
                            
                       // }.padding(.leading, 30)
                          //  .onTapGesture {
                                
                                
                      //  }
                        
                      //  NavigationLink(destination: RegistrationView(), tag: 2, selection: $selection) {
                            
                            Text("Men")
                                .modifier(GSTextStyle(textColor: .black, fontWeight: .light, fontSize: 16)).padding(.leading, 10)
                       // }.padding(.leading, 30)
                           // .onTapGesture {
                                
                                
                      //  }
                        
                    }.padding(.top, 50)
                    .padding(.leading, 30)
                    
                    VStack(alignment: .leading) {
                        Text("Use sunlight, no make-up, no filters, no group photos, simple outfit.")
                            .font(.system(size: 14))
                            .foregroundColor(.gray)
                            .padding([.leading], 35)
                            .padding(.top, 10)
                    }
                    
                    HStack {
                        
                        Image("Group 10984")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding([.leading, .trailing], 30)
                        Spacer()
                        Image("Group 11061")
                            
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding([.leading, .trailing], 30)
                        
                    }
                    HStack {
                        Image("Group 11061")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding([.leading, .trailing], 30)
                        Spacer()
                        Image("Group 10984")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding([.leading, .trailing], 30)
                    }
                    HStack {
                        Image("Group 10984")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding([.leading, .trailing], 30)
                        Spacer()
                        Image("Group 10984")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 0)
                            .padding([.leading, .trailing], 30)
                        
                        
                    }
                    
                    VStack(alignment: .center) {
                        NavigationLink(destination: UploadImageView()) {
                            ForwardIcon(height: 70, cornerRadius: 35)
                        }.foregroundColor(.white)
                    }
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.top, 30)
                    .padding(.bottom,15)
                }
            }
            
            
        }
        .hideNavigationBar()
        .background(BGColor())
    }
}

struct ExamplePhotosView_Previews: PreviewProvider {
    static var previews: some View {
        ExamplePhotosView()
    }
}
