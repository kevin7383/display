//
//  ScoutYourFriendVC.swift
//  geoscout
//
//  Created by Smit Kukadiya on 15/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct ScoutYourFriendVC: View {
    @EnvironmentObject var navigator: ScreenNavigator
    @State var selection: Int? = 0

    var body: some View {
        VStack(alignment: .leading) {
            ScrollView(.vertical, showsIndicators: false) {
                
                    
                VStack(alignment: .leading) {
                    HStack{
                        Spacer()
                                                                   Button(action: {}) {
                                                                       Image("ThreeDot")
                                                                           .foregroundColor(.white)
                                                                         .padding(.bottom, 310)
                                                                        .padding(.trailing, 40)
                                                                      .frame(width: 50, height: 80)
                                                                       
                                                                   }
                    }
                   .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    .background(
                    Image("lady1")
                        .resizable()
                        .frame(width: 415, height: 450))
                    
                }
                    
                .frame(minWidth: 0, maxWidth: .infinity)
                .frame(height: 450)
                .padding(.top, 0)
            
                
                VStack(alignment: .leading) {
                    Image("bottom_Shaow")
                        .resizable()
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding(.top, -35)
                        .padding(.bottom,-50)
                        .padding([.leading, .trailing], 0)
                    Text("Scout your friend")
                        .font(.system(size: 30))
                        .foregroundColor(.GSPrimary)
                        .padding([.leading], 35)
                        .padding(.top, -420)
                    GenderFriendSelectionControl().padding(.top, -340)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 0)
                Divider()
                Group {
                    textFieldViewScoutYourName()
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding(.top, -290)
                        .padding(.leading, 20)
                        .padding(.trailing, 20)
                    
                   
                    
                    VStack(alignment: .center) {
                         NavigationLink(destination: Sharelink(), tag: 1, selection: $selection) {
                        Button(action: {
                            self.selection = 1
                        }) {
                            Text("SCOUT")
                                .foregroundColor(.white)
                              //  .padding(.all, 10.0)
                                .font(.system(size: 18))
                            
                        }
                         }
                        
                        .frame(width: 220, height: 55)
                        .background(Color.GSPrimary)
                        .cornerRadius(20.0)
                        //.padding(.top,-40)
                        
                    }//.padding(.bottom,-90)
                        .padding(.bottom,70)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.top, -130)
                }
            }
            
        }
        .hideNavigationBar()
        .background(BGColor())
    }
    
}

struct textFieldViewScoutYourName: View {
    
    @State var EyeColor: String = ""
    
    var body: some View {
        HStack(alignment: .top) {
            
            TextField("Name", text: $EyeColor, onEditingChanged: { changed in
                print("Username onEditingChanged - \(changed)")
            }) {
                print("Username onCommit")
            }.padding(.top, 0)
                .padding([.leading, .trailing], 20)
                .frame(height: 80)
                .font(Font.system(size: 20, weight: .medium, design: .default))
            
        }
        .frame(minWidth: 0, maxWidth: .infinity)
     //   .padding(.top,15)
        .frame(height: 60)
        .background(Color.white)
        .overlay(
            RoundedRectangle(cornerRadius: 5)
                .stroke(Color.gray, lineWidth: 0.5)
        )
            .hideNavigationBar()
    }
}

struct ScoutYourFriendVC_Previews: PreviewProvider {
    static var previews: some View {
        ScoutYourFriendVC()
    }
}
