//
//  ShareScoutingLink4.swift
//  geoscout
//
//  Created by Smit Kukadiya on 15/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct ShareScoutingLink4: View {
    @EnvironmentObject var navigator: ScreenNavigator
    @State var selection: Int? = 0
    
    var body: some View {
        VStack(alignment: .center) {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center) {
                    Text("Do you know\n someone that could\n be a model too?")
                        .font(.system(size: 30))
                        .foregroundColor(Color.white)
                        .padding([.leading, .trailing], 0)
                        .padding(.top, 25)
                        .padding(.bottom, 25)
                        .multilineTextAlignment(.center)
                        .lineLimit(nil)
                    
                    Image("Ellipse 3264")
                        .resizable()
                        .frame(width: 230, height: 230)
                        .padding(.top, 0)
                        .background(Color.GSPrimary)
                    
                    Divider()
                    
                    Text("Earn up to 250 euro per\n model you scout!")
                        .font(.system(size: 22))
                        .foregroundColor(Color.white)
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 18)
                        .padding(.bottom, 20)
                        .multilineTextAlignment(.center)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .frame(height: 550)
                .background(Color.GSPrimary)
                .cornerRadius(40, corners: [.bottomLeft, .bottomRight])
                .padding(.top, 0)
                .padding([.leading, .trailing], 0)
                VStack(alignment: .center) {
                    Text("Don’t wait until someone\n else scouts him or her. ")
                        .font(.system(size: 24
                            ))
                        .foregroundColor(Color(red: 61/255, green: 59/255, blue: 83/255))
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 60)
                        .padding(.bottom, 10)
                        .multilineTextAlignment(.center)
                }
                
          
                
                VStack(alignment: .center) {
                    NavigationLink(destination: ScoutModelView(), tag: 1, selection: $selection) {
                        Button(action: {
                            self.selection = 1
                        }) {
                            Text("Scout a friend")
                                .foregroundColor(.white)
                                .padding(.all, 10.0)
                                .font(.system(size: 18))
                        } .frame(width: 220, height: 55)
                            .background(Color.GSPrimary)
                            .cornerRadius(20.0)
                    }
                    
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 30)
                .padding(.bottom, 35)
            }
        }
        .hideNavigationBar()
        .background(BGColor())
    }
}

struct ShareScoutingLink4_Previews: PreviewProvider {
    static var previews: some View {
        ShareScoutingLink4()
    }
}
