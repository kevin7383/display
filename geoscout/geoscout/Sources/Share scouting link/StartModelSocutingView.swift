//
//  StartModelSocutingView.swift
//  geoscout
//
//  Created by Smit Kukadiya on 17/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct StartModelSocutingView: View {
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .center) {
            NavigationBarDotOrange()
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading) {
                    Image("ic_StartModel")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding(.top, -60)
                        .padding([.leading, .trailing], 0)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .frame(height: 250)
                .padding(.top, 0)
                
                VStack(alignment: .center) {
                    Text("If 5 or more agencies want\n to contract the model")
                        .font(.system(size: 16))
                        .foregroundColor(Color(red: 112/255, green: 112/255, blue: 112/255))
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 50)
                        .padding(.bottom, 20)
                        .multilineTextAlignment(.center)
                }
                VStack(alignment: .center) {
                    Text("€ 250,00")
                        .bold()
                        .font(.system(size: 36))
                        .foregroundColor(Color.GSPrimary)
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 0)
                        .padding(.bottom, 0)
                        .multilineTextAlignment(.center)
                    
                    Text("Reward")
                        .font(.system(size: 36))
                        .foregroundColor(Color.GSPrimary)
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 0)
                        .padding(.bottom, 0)
                        .multilineTextAlignment(.center)
                }
                
                VStack(alignment: .center) {
                    NavigationLink(destination: ScoutYourFriendVC()) {
                        ForwardIcon(height: 70, cornerRadius: 35)
                    }.foregroundColor(.white)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 20)
                .padding(.bottom, 35)
                   
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct StartModelSocutingView_Previews: PreviewProvider {
    static var previews: some View {
        StartModelSocutingView()
    }
}
