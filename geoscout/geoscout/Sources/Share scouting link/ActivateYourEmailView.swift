//
//  ActivateYourEmailView.swift
//  geoscout
//
//  Created by Smit Kukadiya on 16/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct ActivateYourEmailView: View {
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .center) {
            NavigationBarDotOrange()
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading) {
                    Image("Image 133")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding(.top, 0)
                        .padding([.leading, .trailing], 0)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .frame(height: 250)
                .padding(.top, 0)
                VStack(alignment: .center) {
                    Text("ACTIVATE\n YOUR EMAIL")
                        .bold()
                        .font(.system(size: 23 ))
                        .foregroundColor(Color.GSPrimary)
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 0)
                        .padding(.bottom, 0)
                        .multilineTextAlignment(.center)
                }
                VStack(alignment: .center) {
                    Text("You must confirm your e-mailadres to get full\n access. That way we are sure that to get in touch\n with you!\n\n\n\n Also check your spambox.")
                        .font(.system(size: 12))
                        .foregroundColor(Color(red: 61/255, green: 59/255, blue: 83/255))
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 40)
                        .padding(.bottom, 20)
                        .multilineTextAlignment(.center)
                }
                
                VStack(alignment: .center) {
                    Button(action: {}) {
                     Text("RESEND EMAIL")
                         .foregroundColor(.white)
                     .font(.system(size: 14))
                     }
                     .frame(width: 300, height: 55)
                     .background(Color(red: 255/255, green: 179/255, blue: 108/255))
                     .cornerRadius(22.0)
                 }
                 .frame(minWidth: 0, maxWidth: .infinity)
                 .padding(.top, 115)
                
                VStack(alignment: .center) {
                   Button(action: {}) {
                    Text("CONTINUE TO SITE")
                        .foregroundColor(.white)
                    .font(.system(size: 14))
                    }
                    .frame(width: 300, height: 55)
                    .background(Color(red: 238/255, green: 92/255, blue: 35/255))
                    .cornerRadius(22.0)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 10)
                .padding(.bottom, 15)
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct ActivateYourEmailView_Previews: PreviewProvider {
    static var previews: some View {
        ActivateYourEmailView()
    }
}
