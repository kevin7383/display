
import SwiftUI

struct NavigationBar: View {
    var showMore: Bool = false
    @Environment(\.presentationMode) var presentation
    var body: some View {
        HStack {
            Button(action: {
                self.presentation.wrappedValue.dismiss()
            }) {
                Image("backArrow")
                    .foregroundColor(.GSPrimary).padding()
            }
            Spacer()
            if showMore {
                Button(action: {}) {
                    Image("ThreeDot")
                        .foregroundColor(.GSPrimary).padding()
                }
                .padding(.trailing, 5)
            }
        }
    }
}

struct NavigationBarDotOrange: View {
    var showMore: Bool = false
    @Environment(\.presentationMode) var presentation
    var body: some View {
        HStack {
            Button(action: {
                self.presentation.wrappedValue.dismiss()
            }) {
                Image("backArrow")
                    .foregroundColor(.GSPrimary).padding()
            }
            Spacer()
            Button(action: {}) {
                Image("ThreeDot")
                    .foregroundColor(.GSPrimary).padding()
                    
            }
            .padding(.trailing, 5)
        }
    }
}

struct NavigationBar_Previews: PreviewProvider {
    static var previews: some View {
        GSTitleNavigationBar()
    }
}

struct NavigationBarDot: View {
    var showMore: Bool = false
    @Environment(\.presentationMode) var presentation
    var body: some View {
        HStack {
            Button(action: {
                self.presentation.wrappedValue.dismiss()
            }) {
                Image("backArrow")
                    .foregroundColor(.white).padding()
            }
            Spacer()
            Button(action: {}) {
                Image("ThreeDot")
                    .foregroundColor(.white).padding()
            }
            .padding(.trailing, 5)
        }
    }
}

struct GSNavigationBar: View {
    var showMore: Bool = true
    
    @Environment(\.presentationMode) var presentation
    var body: some View {
        HStack {
            Button(action: {
                self.presentation.wrappedValue.dismiss()
            }) {
                Image("backArrow")
                    .foregroundColor(.GSPrimary).padding()
            }
            Spacer()
            if showMore {
                Button(action: {}) {
                    Image(systemName: "text.justifyleft")
                        .foregroundColor(.GSPrimary).padding()
                }
                .padding(.trailing, 5)
            }
        }
    }
}

struct GSTitleNavigationBar: View {
    @State var selection: Int? = 0
    @EnvironmentObject var navigator: ScreenNavigator
@Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var body: some View {
        
        HStack {
            RegularText(text: "gescout", textColor: .GSPurpleGray, fontSze: 18)
                .padding(.leading, 20).onTapGesture {
                    
                    self.presentationMode.wrappedValue.dismiss()
            }
            Spacer()
            
            NavigationLink(destination: SideMenuView()) {
                                   SideMenuButton()

                }.padding(.trailing, 5)
            
        }.frame(height: 70)
    
    }
}
