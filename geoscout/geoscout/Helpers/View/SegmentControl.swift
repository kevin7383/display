

import SwiftUI

struct GenderSelectionControl: View {
    @State var selectionIndex = 0

    var body: some View {
        HStack(alignment: .bottom, spacing: 0) {
            Button(action: {}) {
                Text("FEMALE")
                    .frame(maxWidth: .infinity)
                     .font(.system(size: 8))
                    .padding()
                    .foregroundColor(selectionIndex == 0 ? .white : .GSDarkText)
                    .background(selectionIndex == 0 ? .GSPrimary : Color.clear)
            }
            .cornerRadius(5)
            .clipped()
            Button(action: {}) {
                Text("MALE")
                    .frame(maxWidth: .infinity)
                    .font(.system(size: 8))
                    .padding()
                    .foregroundColor(selectionIndex == 1 ? .white : .GSDarkText)
                    .background(selectionIndex == 1 ? .GSPrimary : Color.clear)
            }
        }

        .cornerRadius(5)
        .clipped()
    }
}

struct GenderFriendSelectionControl: View {
    @State var selectionIndex = 0

    var body: some View {
        HStack(alignment: .bottom, spacing: 10) {
            Button(action: {}) {
                Text("Girl")
                    .frame(maxWidth: .infinity)
                    .padding()
                    .foregroundColor(selectionIndex == 0 ? .white : .GSDarkText)
                    .background(selectionIndex == 0 ? .GSPrimary : Color(red: 238/255, green: 238/255, blue: 238/255))
            }
            .cornerRadius(5)
            .clipped()
            .padding(.leading, 35)
            Button(action: {}) {
                Text("boy")
                    .frame(maxWidth: .infinity)
                    .padding()
                    .foregroundColor(selectionIndex == 1 ? .white : .GSDarkText)
                    .background(selectionIndex == 1 ? .GSPrimary : Color(red: 238/255, green: 238/255, blue: 238/255))
            }
            .padding(.trailing, 35)
            .cornerRadius(5)
            .clipped()
        }

        
    }

}

struct SegmentControl_Previews: PreviewProvider {
    static var previews: some View {
        GenderSelectionControl()
    }
}
