//
//  Changepassword.swift
//  geoscout
//
//  Created by SMIT KUKADIYA on 16/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct Changepassword: View {
    @State var email: String = ""
    @State var password: String = ""
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .center) {
            NavigationBarDotOrange()
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                
                
                VStack(alignment: .leading) {
                    Text("Change Password")
                        .bold()
                        .font(.system(size: 22 ))
                        .foregroundColor(Color.GSPrimary)
                        .padding(.trailing,140)
                        .padding(.top, 20)
                        
                        .multilineTextAlignment(.leading)
                }
                VStack(alignment: .leading) {
                    Text("Please enter your current password and\n your new one.")
                        .font(.system(size: 16))
                        .foregroundColor(Color(red: 61/255, green: 59/255, blue: 83/255))
                        .padding(.trailing,50)
                        .padding(.top, 40)
                       
                        .multilineTextAlignment(.leading)
                }
                
                
                 VStack(alignment: .leading) {
                                        Text("Current Password")
                                        .font(.system(size: 14))
                                            .padding(.leading,40)
                                            .padding(.top,50)
                    HStack{
                        
                        TextField("******************", text: $email, onEditingChanged: { changed in
                                                                   print("Username onEditingChanged - \(changed)")
                                                                              }) {
                                                                   print("Username onCommit")
                                                               }.padding(.top, 10)
                                                    .padding(.leading, 40)
                        
                        
                         Spacer()
                        
                                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                                                Image("eye")
                                                    .padding(.trailing,40)
                                                    .foregroundColor(Color.GSPrimary)
                        }
                    }
                }
                
           
                Divider()
                
                
                VStack(alignment: .leading) {
                                        Text("New Password")
                                        .font(.system(size: 14))
                                            .padding(.leading,40)
                                            .padding(.top,0)
                    HStack{
                        
                        TextField("******************", text: $password, onEditingChanged: { changed in
                                                                   print("Username onEditingChanged - \(changed)")
                                                                              }) {
                                                                   print("Username onCommit")
                                                               }.padding(.top, 10)
                                                    .padding(.leading, 40)
                        
                        
                         Spacer()
                        
                                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                                                Image("eye")
                                                    .padding(.trailing,40)
                                                    .foregroundColor(Color.GSPrimary)
                        }
                    }
                }
                
                
                
                
                
//                VStack(alignment: .center) {
//                    Button(action: {}) {
//                     Text("RESEND EMAIL")
//                         .foregroundColor(.white)
//                     .font(.system(size: 18))
//                     }
//                     .frame(width: 300, height: 55)
//                     .background(Color(red: 255/255, green: 179/255, blue: 108/255))
//                     .cornerRadius(22.0)
//                 }
//                 .frame(minWidth: 0, maxWidth: .infinity)
//                 .padding(.top, 240)
                
                 Button(action: {}) {
                                           Text("SAVE")
                    .bold()
                                               .foregroundColor(.white)
                                               .padding(.all, 10.0)
                                               .font(.system(size: 14))
                                       }
                                       .frame(width: 220, height: 55)
                                       .background(Color.GSPrimary)
                                       .cornerRadius(20.0)
                 .padding(.top, 50)
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct Changepassword_Previews: PreviewProvider {
    static var previews: some View {
        Changepassword()
    }
}
