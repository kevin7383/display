//
//  Invitation.swift
//  geoscout
//
//  Created by SMIT KUKADIYA on 17/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct Invitation: View {
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .center) {
            
            
            //            NavigationBarDotOrange()
            //                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                
                
                
                
                
                ZStack() {
                    
                    Image("sds")
                        .resizable()
                        .scaledToFill()
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding(.top, 0)
                        .padding([.leading, .trailing], 0)
                    VStack(alignment: .center){
                        
                        Text("You have got 5 \n     invitations!")
                                           .foregroundColor(Color.black)
                                           .font(.system(size: 30))
                                               .padding(.top, -90)
                    }
                   
                    HStack {
                        Button(action: {
                        }) {
                            Image("backArrow")
                                .foregroundColor(.GSPrimary).padding()
                        }
                        Spacer()
                        Button(action: {}) {
                            Image("ThreeDot")
                                .foregroundColor(.GSPrimary).padding()
                        }
                       
                        .padding(.trailing, 5)
                        
                    }.padding(.top, -165)
                                   
                    
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .frame(height: 350 )
                .padding(.top, 0)
                
               
                
                VStack(alignment: .center){
                    VStack(alignment: .leading) {
                        Image("modell")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 330)
                            .padding(.leading, 18)
                    }
                    .frame(width: 303, height: 303)
                    .padding(.top, -120)
                    
                }
                
                
                VStack(alignment: .center) {
                    Button(action: {}) {
                        Text("READ INTERVIEW")
                            .foregroundColor(.gray)
                            .font(.system(size: 18))
                    }
                    .frame(width: 300, height: 55)
                    .background(Color(red: 238/255, green: 233/255, blue: 245/255))
                    .cornerRadius(22.0)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 150)
                
                VStack(alignment: .center) {
                    Button(action: {}) {
                        Text("PLAN CASTING")
                            .foregroundColor(.white)
                            .font(.system(size: 18))
                    }
                    .frame(width: 300, height: 55)
                    .background(Color(red: 238/255, green: 92/255, blue: 35/255))
                    .cornerRadius(22.0)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 10)
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct Invitation_Previews: PreviewProvider {
    static var previews: some View {
        Invitation()
    }
}
