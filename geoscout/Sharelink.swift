//
//  Sharelink.swift
//  geoscout
//
//  Created by SMIT KUKADIYA on 16/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct Sharelink: View {
    @State var EyeColor: String = ""
    @EnvironmentObject var navigator: ScreenNavigator
    @State var selection: Int? = 0
    var body: some View {
        VStack(alignment: .center) {
            NavigationBarDotOrange()
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                
                
                VStack(alignment: .center) {
                    Text("Share this link with\n Jane!")
                        .bold()
                        .font(.system(size: 30 ))
                        .foregroundColor(Color.black)
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 0)
                        .padding(.bottom, 0)
                    .lineLimit(nil)
                        .multilineTextAlignment(.center)
                }
                
                
                VStack(alignment: .center) {
                    
                    ZStack(){
                        
                        
                        Image("refer")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding(.top, 330)
                            .padding(.leading, 18)
                        
                        Text("Earn up to \n250 euro*")
                            .bold()
                            .font(.system(size: 30))
                            .frame(width: 300, height: 100) .foregroundColor(Color.GSPrimary)
                            .padding(.top, 350)
                            .padding(.leading, 40)
                        HStack(alignment: .center){
                            
                            SharelinkView()
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .padding(.top, 510)
                                .padding(.leading, 20)
                                .padding(.trailing, 20)
                            
                            Spacer()
                            
                            
                            
                            
                        }
                        
                        
                    }
                    
                    
                    
                }
                    
                .frame(width: 400, height: 600)
                .frame(height: 543)
                .padding(.top, -230)
                
                
                //                VStack(alignment: .center) {
                //                    Button(action: {}) {
                //                     Text("RESEND EMAIL")
                //                         .foregroundColor(.white)
                //                     .font(.system(size: 18))
                //                     }
                //                     .frame(width: 300, height: 55)
                //                     .background(Color(red: 255/255, green: 179/255, blue: 108/255))
                //                     .cornerRadius(22.0)
                //                 }
                //                 .frame(minWidth: 0, maxWidth: .infinity)
                //                 .padding(.top, 240)
                
                
                Text("We will notify you when Jane has\n submitted her photos and what the \n outcome is!")
                    
                    .font(.system(size: 16))
                    .foregroundColor(Color.gray)
                    .padding(.top, 30)
                
               
                
                VStack(alignment: .center) {
                     NavigationLink(destination: ScoutModelView(), tag: 1, selection: $selection) {
                    Button(action: {
                        self.selection = 1
                    }) {
                        Text("ANOTHER MODEL")
                            .foregroundColor(.white)
                            .font(.system(size: 18))
                    }
                    .frame(width: 315, height: 55)
                    .background(Color(red: 238/255, green: 92/255, blue: 35/255))
                    .cornerRadius(22.0)
                }
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top,50)
                .padding(.bottom, 10)
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct Sharelink_Previews: PreviewProvider {
    static var previews: some View {
        Sharelink()
    }
}
struct SharelinkView: View {
    
    @State var EyeColor: String = ""
    
    var body: some View {
        HStack(alignment: .top) {
            
            TextField("Share link", text: $EyeColor, onEditingChanged: { changed in
                print("Username onEditingChanged - \(changed)")
            }) {
                print("Username onCommit")
            }.padding(.top, 0)
                .padding(.leading, 30)
                .padding(.trailing,00)
                .frame(height: 56)
                .font(Font.system(size: 20, weight: .medium, design: .default))
            
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                Image("copy")
                    .foregroundColor(Color.black)
                    .padding(.top, 12)
                    .padding(.trailing, 10)
                   
                
            }
            
            
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .frame(height: 56)
        .background(Color.white)
        .overlay(
            RoundedRectangle(cornerRadius: 5)
                .stroke(Color.gray, lineWidth: 0.5)
        )
            .hideNavigationBar()
    }
}
