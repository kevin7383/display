//
//  MyCasting.swift
//  geoscout
//
//  Created by SMIT KUKADIYA on 16/06/20.
//  Copyright © 2020 Ajay Mehra. All rights reserved.
//

import SwiftUI

struct MyCasting: View {
    @EnvironmentObject var navigator: ScreenNavigator
    var body: some View {
        VStack(alignment: .center) {
            NavigationBarDotOrange()
                .frame(height: 60)
            ScrollView(.vertical, showsIndicators: false) {
                
                
                VStack(alignment: .center) {
                    Text("My casting")
                        .bold()
                        .font(.custom("SFProDisplay-Regular", size: 27))
                        .foregroundColor(Color.GSPrimary)
                        .padding([.leading, .trailing], 20)
                        .padding(.top, 20)
                        .padding(.bottom, 0)
                        .multilineTextAlignment(.center)
                }
                
               Group {
                   Text("You are currently being introduced. The\ncasting ends on ")
                       .fontWeight(.regular) +
                   Text("Saturday 12:00")
                       .fontWeight(.bold)
                 
               }  .multilineTextAlignment(.center)
                 .font(.custom("SFProDisplay-Regular", size: 16))
                 .padding(.top, 80)
                
                VStack(alignment: .leading) {
                                   Image("mycasting")
                                       .frame(minWidth: 0, maxWidth: .infinity)
                                       .padding(.top, 330)
                                    .padding(.leading, 38)
                               }
                              .frame(width: 400, height: 600)
                               .frame(height: 543)
                               .padding(.top, -195)
                
                
//                VStack(alignment: .center) {
//                    Button(action: {}) {
//                     Text("RESEND EMAIL")
//                         .foregroundColor(.white)
//                     .font(.system(size: 18))
//                     }
//                     .frame(width: 300, height: 55)
//                     .background(Color(red: 255/255, green: 179/255, blue: 108/255))
//                     .cornerRadius(22.0)
//                 }
//                 .frame(minWidth: 0, maxWidth: .infinity)
//                 .padding(.top, 240)
                
                VStack(alignment: .center) {
                   Button(action: {}) {
                    Text("READ INTERVIEW")
                        .foregroundColor(.white)
                        .font(.custom("SFProDisplay-Regular", size: 18))                    }
                    .frame(width: 300, height: 55)
                    .background(Color(red: 238/255, green: 92/255, blue: 35/255))
                    .cornerRadius(22.0)
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .padding(.top, 150)
                .padding(.bottom,40)
            }
        }
        .hideNavigationBar()
        .background(BGColor())
        
    }
}

struct MyCasting_Previews: PreviewProvider {
    static var previews: some View {
        MyCasting()
    }
}
